package org.elsa.fingobtb.api.response;

/**
 * 只带有一个属性的VO
 **/
public class GeneralResult<T> extends BaseResult {
    private T value;

    public GeneralResult(boolean success, String message, T value) {
        this.setSuccess(success);
        this.setMessage(message);
        this.setValue(value);
    }

    public GeneralResult<T> setValue(T value) {
        this.value = value;
        return this;
    }

    public T getValue() {
        return value;
    }
}

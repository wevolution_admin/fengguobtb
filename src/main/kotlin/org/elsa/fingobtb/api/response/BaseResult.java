package org.elsa.fingobtb.api.response;

public class BaseResult {
	public static final BaseResult SUCCESS;
	public static final BaseResult FAIL;

	private boolean success;
	private String message;

	static {
		SUCCESS = new BaseResult();
		FAIL = new BaseResult();
		FAIL.setSuccess(false);
	}

	public BaseResult() {
		this.setSuccess(true);
		this.message = "";
	}

	public BaseResult(boolean success, String message) {
		this.setSuccess(success);
		this.setMessage(message);
	}

	public BaseResult setSuccess(boolean success) {
		this.success = success;
		return this;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}

	public BaseResult setMessage(String message) {
		this.message = message;
		return this;
	}
}

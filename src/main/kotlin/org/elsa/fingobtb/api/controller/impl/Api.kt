package org.elsa.fingobtb.api.controller.impl

import org.apache.commons.lang3.StringUtils
import org.elsa.fingobtb.api.controller.AbstractApi
import org.elsa.fingobtb.api.response.BaseResult
import org.elsa.fingobtb.common.func.BtbRestfulOption
import org.elsa.fingobtb.common.func.BtbRestfulOption.getMarkets
import org.elsa.fingobtb.service.Buy
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RestController

/**
 * @author valord577
 * @date 2018/7/17 19:28
 */
@RestController
class Api @Autowired constructor(val buyCoin: Buy) : AbstractApi() {

    override fun getTicker(symbol: String): String? {
        return BtbRestfulOption.getTicker(symbol)
    }

    override fun getBook(symbol: String, size: Int?): String? {
        return BtbRestfulOption.book(symbol, size)
    }

    override fun getCandles(symbol: String, type: String, size: Int?, start: Int?): String? {
        return BtbRestfulOption.candles(symbol, type, size, start)
    }

    override fun buy(symbol: String?, price: Int): BaseResult {
        if (StringUtils.isAllBlank(symbol)) {
            return buyCoin.buyLimit("wecc", price)
        }

        return BaseResult(false, "暂未开放wecc以外的币种交易")
    }

    override fun sell(symbol: String, price: String, amount: String): String? {
        return BtbRestfulOption.sell(symbol, price, amount)
    }

    override fun cancel(symbol: String, orderId1: String, orderId2: String?, orderId3: String?): String? {
        var orderId = orderId1
        if (null != orderId2) {
            orderId = "$orderId,$orderId2"
        }

        if (null != orderId3) {
            orderId = "$orderId,$orderId3"
        }

        return BtbRestfulOption.cancel(symbol, orderId)
    }

    override fun getFunds(): String? {
        return BtbRestfulOption.getFunds()
    }

    override fun getHisOrder(symbol: String, status: Int, currentPage: Int, pageLength: Int): String? {
        return BtbRestfulOption.getHisOrder(symbol, status, currentPage, pageLength)
    }

    override fun getOrderInfo(symbol: String, orderId: Long): String? {
        return BtbRestfulOption.getOrderInfo(symbol, orderId)
    }

    override fun getCoinTrades(symbol: String): String? {
        return BtbRestfulOption.getCoinTrades(symbol)
    }

    override fun getCoinTotal(): String? {
        return BtbRestfulOption.getCoinTotal()
    }

}
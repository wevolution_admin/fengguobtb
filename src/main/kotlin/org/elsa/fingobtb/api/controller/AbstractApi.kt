package org.elsa.fingobtb.api.controller

import io.swagger.annotations.*
import org.elsa.fingobtb.api.response.BaseResult
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

/**
 * @author valord577
 * @date 2018/7/18 15 06
 */
@RestController
@Api(tags = ["AbstractApi"], description = "关于btb交易操作的api")
abstract class AbstractApi {

    /**
     * 行情
     *
     * @param symbol 交易对 如eth_btc
     * @return 返回值请查阅Btb文档
     */
    @ApiOperation(value = "查看行情")
    @ApiImplicitParam(name = "symbol", value = "交易对", required = true, paramType = "query", defaultValue = "wecc_eth")
    @RequestMapping(value = ["/ticker"], method = [(RequestMethod.GET)])
    abstract fun getTicker(symbol: String): String?

    /**
     * 深度
     *
     * @param symbol 交易对 如eth_btc
     * @param size CAN NULL 默认200 取值1-200
     * @return  asks 卖方深度 | bids 买方深度
     */
    @RequestMapping(value = ["/book"], method = [(RequestMethod.GET)])
    abstract fun getBook(symbol: String, size: Int?): String?

    /**
     * k线
     *
     * @param symbol 交易对 如eth_btc
     * @param type   1min/3min/5min/15min/30min/1day/3day/1week/1hour/2hour/4hour/6hour
     * @param size   CAN NULL 指定获取数据的条数 默认全部获取
     * @param start  CAN NULL 时间戳 返回该时间戳之后的全部数据 默认返回全部数据
     * @return 返回值请查阅Btb文档
     */
    @RequestMapping(value = ["/candles"], method = [(RequestMethod.GET)])
    abstract fun getCandles(symbol: String, type: String, size: Int?, start: Int?): String?

    /**
     * 下单 买 限价单
     *
     * @param symbol 交易对 如wecc
     * @param price 下单价格
     * @return {"result":true,"order_id":123456}   result:true 表示成功 | order_id:􏱸􏱴ID 订单ID
     */
    @ApiOperation(value = "买进货币")
    @ApiImplicitParams(
            ApiImplicitParam(name = "symbol", value = "交易对", required = false, paramType = "query", defaultValue = ""),
            ApiImplicitParam(name = "price", value = "购买总价(RMB 整数 单位分)", required = true, paramType = "query", defaultValue = "100")
    )
    @RequestMapping(value = ["/buy"], method = [(RequestMethod.GET)])
    abstract fun buy(symbol: String?, price: Int): BaseResult

    /**
     * 下单 卖 限价单
     *
     * @param symbol 交易对 如eth_btc
     * @param price 下单价格
     * @param amount 交易数量
     * @return 同 /buy 接口  详情参考Btb文档
     */
    @ApiOperation(value = "卖出货币")
    @RequestMapping(value = ["/sell"], method = [(RequestMethod.GET)])
    abstract fun sell(symbol: String, price: String, amount: String): String?

    /**
     * 撤单
     *
     * @param symbol 交易对 如eth_btc
     * @param orderId1 订单号1
     * @param orderId2 CAN NULL 订单号2
     * @param orderId3 CAN NULL 订单号3
     * @return 返回值请查阅Btb文档
     */
    @RequestMapping(value = ["/cancel"], method = [(RequestMethod.GET)])
    abstract fun cancel(symbol: String, orderId1: String, orderId2: String?, orderId3: String?): String?

    /**
     * 查资金
     *
     * @return 返回值请查阅Btb文档
     */
    @RequestMapping(value = ["/funds"], method = [(RequestMethod.GET)])
    abstract fun getFunds(): String?

    /**
     * 查委托 (查历史订单)
     *
     * @param symbol 交易对 如eth_btc
     * @param status 查询状态 0：未完成订单 | 1：已完成订单 (最近两天对数据)
     * @param currentPage 当前页数
     * @param pageLength 每夜数据条数 最多不超过200
     * @return 返回值请查阅Btb文档
     */
    @RequestMapping(value = ["/history/order"], method = [(RequestMethod.GET)])
    abstract fun getHisOrder(symbol: String, status: Int, currentPage: Int, pageLength: Int): String?

    /**
     * 获取用户某一交易对的 订单信息
     *
     * @param symbol 交易对 如eth_btc
     * @param orderId 订单号
     * @return 返回值请查阅Btb文档
     */
    @RequestMapping(value = ["/orderInfo"], method = [(RequestMethod.GET)])
    abstract fun getOrderInfo(symbol: String, orderId: Long): String?

    /**
     * 获取某个交易币对的交易信息
     *
     * @param symbol 交易对 如eth_btc
     * @return 返回值请查阅Btb文档
     */
    @RequestMapping(value = ["/coinTrades"], method = [(RequestMethod.GET)])
    abstract fun getCoinTrades(symbol: String): String?

    /**
     * 获取交易币对信息
     *
     * @return 返回值请查阅Btb文档
     */
    @RequestMapping(value = ["/coinTotal"], method = [(RequestMethod.GET)])
    abstract fun getCoinTotal(): String?

}

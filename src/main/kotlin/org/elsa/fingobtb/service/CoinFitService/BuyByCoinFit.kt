package org.elsa.fingobtb.service.CoinFitService

import org.elsa.fingobtb.api.response.BaseResult
import org.elsa.fingobtb.entity.request.CreateOrder

/**
 * btb买入操作
 *
 * @author valord577
 * @date 2018/7/19 17:29
 */
interface BuyByCoinFit {

    /**
     * 按限价单
     */
    fun buyLimit(symbol: String, price: Int, co: CreateOrder): BaseResult

}
package org.elsa.fingobtb.service

import org.elsa.fingobtb.api.response.BaseResult

/**
 * btb买入操作
 *
 * @author valord577
 * @date 2018/7/19 17:29
 */
interface Buy {

    /**
     * 按限价单
     */
    fun buyLimit(symbol: String, price: Int): BaseResult

}
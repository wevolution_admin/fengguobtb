package org.elsa.fingobtb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class FingoBtbApplication

fun main(args: Array<String>) {
    runApplication<FingoBtbApplication>(*args)
    println("====== fingo-btb 启动完成 ======")
}

package org.elsa.fingobtb.job

import com.alibaba.fastjson.JSON
import org.elsa.fingobtb.common.config.Account
import org.elsa.fingobtb.common.func.BtbRestfulOption
import org.elsa.fingobtb.common.utils.SmsApi
import org.elsa.fingobtb.entity.response.FundsData
import org.elsa.fingobtb.entity.response.TickerData
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

/**
 * @author valord577
 * @date 18-7-27 下午4:23
 */
@Service
class FundsJob {

    /**
     * 检测btb账户余额 每十分钟检测一次
     */
    @Scheduled(cron = "0 0/10 * * * ?")
    fun indexFunds() {
        val fundsData = JSON.parseObject(BtbRestfulOption.getFunds(), FundsData::class.java)
        val info = JSON.parseObject(JSON.toJSONString(fundsData.info), FundsData.Info::class.java)
        val funds = JSON.parseObject(JSON.toJSONString(info.funds), FundsData.Info.Founds::class.java)
        val free = JSON.parseObject(JSON.toJSONString(funds.free), FundsData.Info.Founds.Free::class.java)

        //2018.07.27
        val tickerData = JSON.parseObject(BtbRestfulOption.getTicker("eth_usdt"), TickerData::class.java)
        val ticker = JSON.parseObject(JSON.toJSONString(tickerData.ticker), TickerData.Ticker::class.java)

        val ethValuedUSDT = free.eth.toDouble() * ticker.last

        //println("ethValuedUSDT: $ethValuedUSDT")

        //小于100 USDT就可以短信预警了
        /*if (ethValuedUSDT < 100.0) {
            SmsApi.send("btb账户余额较低, ETH余额为${free.eth} " + "【芬果财经】", Account.tipMobile)
        }*/
    }
}
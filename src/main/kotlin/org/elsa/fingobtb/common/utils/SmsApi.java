package org.elsa.fingobtb.common.utils;

import javax.ws.rs.core.MediaType;
import org.slf4j.LoggerFactory;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 *
 * @author Administrator
 */
public class SmsApi {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SmsApi.class);

	/**
	 public static void main(String[] args) {
	 SmsApi api = new SmsApi();
	 String httpResponse = SmsApi.send("验证码：128439【信诺检测】", "13738036100");
	 try {
	 JSONObject jsonObj = new JSONObject().parseObject(httpResponse);
	 int error_code = jsonObj.getIntValue("error");
	 String error_msg = jsonObj.getString("msg");
	 if (error_code == 0) {
	 System.out.println("Send message success.");
	 } else {
	 System.out.println("Send message failed,code is " + error_code + ",msg is " + error_msg);
	 }
	 } catch (Exception ex) {
	 Logger.getLogger(SmsApi.class.getName()).log(Level.SEVERE, null, ex);
	 }

	 httpResponse = api.testStatus();
	 try {
	 JSONObject jsonObj = new JSONObject().parseObject(httpResponse);
	 int error_code = jsonObj.getIntValue("error");
	 if (error_code == 0) {
	 int deposit = jsonObj.getIntValue("deposit");
	 System.out.println("Fetch deposit success :" + deposit);
	 } else {
	 String error_msg = jsonObj.getString("msg");
	 System.out.println("Fetch deposit failed,code is " + error_code + ",msg is " + error_msg);
	 }
	 } catch (Exception ex) {
	 Logger.getLogger(SmsApi.class.getName()).log(Level.SEVERE, null, ex);
	 }

	 }**/

	public static String send(String content, String number) {
		// just replace key here
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("api", "key-d7d21f7db8a477a86d96d5d88bde06f7"));
		WebResource webResource = client.resource("http://sms-api.luosimao.com/v1/send.json");
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		formData.add("mobile", number);
		formData.add("message", content);
		ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
		String textEntity = response.getEntity(String.class);
		// int status = response.getStatus();

		//System.out.print(content);
		//System.out.print(textEntity);
		logger.info("Send sms: " + content + ", to number " + number + ", result is: " + textEntity);
		// System.out.print(status);
		return textEntity;
	}

	private String testStatus() {
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("api", "key-ecc057252e8f2ae4b2bd3d8634b157b3"));
		WebResource webResource = client.resource("http://sms-api.luosimao.com/v1/status.json");
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		ClientResponse response = webResource.get(ClientResponse.class);
		String textEntity = response.getEntity(String.class);
		int status = response.getStatus();
		// System.out.print(status);
		// System.out.print(textEntity);
		return textEntity;
	}

}
package org.elsa.fingobtb.common.utils

import org.apache.http.HttpResponse
import org.apache.http.NameValuePair
import org.apache.http.ParseException
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.util.*

/**
 * @author valord577
 * @date 2018/2/5 11:22
 */
object Https {

    /**
     * get请求
     */
    fun get(url: String): String? {

        val httpClient = HttpClients.createDefault()
        val get = HttpGet(url)
        println(get)
        try {
            val response = httpClient.execute(get)
            return parseResponse(response)
        } catch (e: ClientProtocolException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            httpClient.close()
        }

        return null
    }

    /**
     * post请求 json形式
     */
    fun post(url: String, json: String): String? {

        val httpClient = HttpClients.createDefault()

        val post = createPost(url, json)

        try {
            val response = httpClient.execute(post)
            return parseResponse(response)
        } catch (e: ClientProtocolException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            httpClient.close()
        }

        return null

    }

    /**
     * post请求 from表单形式
     */
    fun post(url: String, params: Map<String, Any>): String? {

        val httpClient = HttpClients.createDefault()
        val post = createPost(url, params)
        try {
            val response = httpClient.execute(post)
            return parseResponse(response)
        } catch (e: ClientProtocolException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            httpClient.close()
        }

        return null

    }

    private fun createPost(url: String, params: Map<String, Any>?): HttpPost {
        val post = HttpPost(url)
        val nvps = ArrayList<NameValuePair>()
//        if (null != params) {
//            for (key in params.keys) {
//                nvps.add(BasicNameValuePair(key, params[key].toString()))
//            }
//        }
        params!!.keys.mapTo(nvps) { BasicNameValuePair(it, params[it].toString()) }
        try {
            post.entity = UrlEncodedFormEntity(nvps, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        return post
    }

    private fun createPost(url: String, json: String): HttpPost {

        val httpPost = HttpPost(url)
        httpPost.setHeader("Accept", "application/json")
        httpPost.setHeader("Content-Type", "application/json")

        val entity = StringEntity(json, "UTF-8")
        httpPost.entity = entity

        return httpPost
    }

    private fun parseResponse(response: HttpResponse): String? {
        val entity = response.entity
        //String charset = EntityUtils.getContentCharSet(entity)
        var body: String? = null
        try {
            body = EntityUtils.toString(entity)
        } catch (e: ParseException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return body
    }
}
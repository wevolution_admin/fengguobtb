package org.elsa.fingobtb.common.func

import org.elsa.fingobtb.entity.request.CancelOrder
import org.elsa.fingobtb.entity.request.DoTrade
import org.elsa.fingobtb.entity.request.HistoricalOrder
import org.elsa.fingobtb.entity.request.OrderInfo
import org.springframework.stereotype.Component

/**
 * @author valord577
 * @date 2018/7/17 15:03
 */
@Component
object BtbRestfulOption : BtbRestfulBase() {

    /**
     * 行情
     */
    fun getTicker(symbol: String): String? {
        return super.ticker(symbol)
    }

    /**
     * 深度 默认size
     */
    fun getBook(symbol: String): String? {
        return getBookOfSize(symbol, null)
    }

    /**
     * 深度 指定size
     */
    fun getBookOfSize(symbol: String, size: Int?): String? {
        return super.book(symbol, size)
    }

    /**
     * k线 不指定数据条数 不指定开始时间（时间戳）
     */
    fun getCandles(symbol: String, type: String): String? {
        return getCandlesOfSizeAndStart(symbol, type, null, null)
    }

    /**
     * k线 不指定数据条数
     */
    fun getCandlesOfSize(symbol: String, type: String, size: Int): String? {
        return getCandlesOfSizeAndStart(symbol, type, size, null)
    }

    /**
     * k线 不指定开始时间（时间戳）
     */
    fun getCandlesOfStart(symbol: String, type: String, start: Int): String? {
        return getCandlesOfSizeAndStart(symbol, type, null, start)
    }

    /**
     * k线
     */
    fun getCandlesOfSizeAndStart(symbol: String, type: String, size: Int?, start: Int?): String? {
        return super.candles(symbol, type, size, start)
    }

    /**
     * 下单
     */
    fun trade(symbol: String, type: String, price: String?, amount: String?): String? {
        val entity = DoTrade(symbol, type, price, amount)
        return super.doTrade(entity)
    }

    /**
     * 下单 买 限价单
     */
    fun buy(symbol: String, price: String, amount: String): String? {
        return trade(symbol, "buy", price, amount)
    }

    /**
     * 下单 卖 限价单
     */
    fun sell(symbol: String, price: String, amount: String): String? {
        return trade(symbol, "sell", price, amount)
    }

    /**
     * 下单 买 市价单
     */
    fun buyMarket(symbol: String, price: String): String? {
        return trade(symbol, "buy_market", price, null)
    }

    /**
     * 下单 卖 市价单
     */
    fun sellMarket(symbol: String, amount: String): String? {
        return trade(symbol, "sell_market", null, amount)
    }

    /**
     * 撤单 多个订单ID中间以","分隔， 一次最多允许撤销三个订单
     */
    fun cancel(symbol: String, orderId: String): String? {
        val entity = CancelOrder(symbol, orderId)
        return super.cancelOrder(entity)
    }

    /**
     * 查资金
     */
    fun getFunds(): String? {
        return super.checkFunds()
    }

    /**
     * 查委托 (查历史订单)
     */
    fun getHisOrder(symbol: String, status: Int, current_page: Int, page_length: Int): String? {
        val entity = HistoricalOrder(symbol, status, current_page, if (page_length > 200) 200 else page_length)
        return super.hisOrder(entity)
    }

    /**
     * 查委托 (查历史订单) 已完成订单 默认第一页 200条
     */
    fun getHisFinishOrder(symbol: String): String? {
        return getHisOrder(symbol, 1, 1, 200)
    }

    /**
     * 查委托 (查历史订单) 已完成订单 自定义页数 条数
     */
    fun getHisFinishOrder(symbol: String, current_page: Int, page_length: Int): String? {
        return getHisOrder(symbol, 1, current_page, page_length)
    }

    /**
     * 查委托 (查历史订单) 未完成订单 默认第一页 200条
     */
    fun getHisNoFinishOrder(symbol: String): String? {
        return getHisOrder(symbol, 0, 1, 200)
    }

    /**
     * 查委托 (查历史订单) 未完成订单 自定义页数 条数
     */
    fun getHisNoFinishOrder(symbol: String, current_page: Int, page_length: Int): String? {
        return getHisOrder(symbol, 0, current_page, page_length)
    }

    /**
     * 获取用户某一交易对的 已完成订单信息
     */
    fun getOrderInfo(symbol: String, orderId: Long): String? {
        val entity = OrderInfo(symbol, orderId)
        return super.orderInfo(entity)
    }

    /**
     * 获取用户某一交易对的 未完成订单信息
     */
    fun getNoFinishOrderInfo(symbol: String): String? {
        return getOrderInfo(symbol, -1L)
    }

    /**
     * 获取某个交易币对的交易信息
     */
    fun getCoinTrades(symbol: String): String? {
        return super.coinTrades(symbol)
    }

    /**
     * 获取交易币对信息
     */
    fun getCoinTotal(): String? {
        return super.coinTotal()
    }

    /**
     * 获取RMB与其他币种的换算
     */
    fun getRmbPrice(): String? {
        return super.rmb2Coin()
    }
}
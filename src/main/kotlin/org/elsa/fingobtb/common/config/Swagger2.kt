package org.elsa.fingobtb.common.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

/**
 * @author valord577生成接口文档
 * @date 2018/7/19 14:40
 */
@Configuration
@EnableSwagger2
class Swagger2 {

    @Bean
    fun createRestApiDocket(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.elsa.fingobtb.api"))
                .paths(PathSelectors.any())
                .build()
    }

    fun apiInfo(): ApiInfo {
        return ApiInfoBuilder()
                .title("WECC Documentation")
                .description("")
                .termsOfServiceUrl("http://www.xn--gtv651d.com/")
                .version("1.0")
                .build()

    }

}
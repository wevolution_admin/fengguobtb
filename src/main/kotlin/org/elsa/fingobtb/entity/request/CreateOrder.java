/*
package org.elsa.fingobtb.entity.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("创建订单-入")
public class CreateOrder {

    @ApiModelProperty("访问密钥")
    private String access_key;

    @ApiModelProperty("时间戳")
    private Long tonce;

    @ApiModelProperty("签名")
    private String signature;

    @ApiModelProperty("市场id")
    private String market;

    @ApiModelProperty("sell或者buy")
    private String side;

    @ApiModelProperty("用户想要出售/购买的金额")
    private String volume;

    @ApiModelProperty("每个单位的价格")
    private String price;

    @ApiModelProperty("ord_type")
    private String ord_type;

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public Long getTonce() {
        return tonce;
    }

    public void setTonce(Long tonce) {
        this.tonce = tonce;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOrd_type() {
        return ord_type;
    }

    public void setOrd_type(String ord_type) {
        this.ord_type = ord_type;
    }
}
*/

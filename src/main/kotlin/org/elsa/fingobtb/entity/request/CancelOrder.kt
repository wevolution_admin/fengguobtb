package org.elsa.fingobtb.entity.request

/**
 * @author valord577
 * @date 2018/7/16 20:07
 */
data class CancelOrder(

        /**
         * 币对 如eth_btc
         */
        val symbol: String,

        /**
         * 订单ID (多个订单ID中间以","分隔， 一次最多允许撤销三个订单)
         */
        val order_id: String
)
package org.elsa.fingobtb.entity.request;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
@ApiModel("Create a withdraw")
public class CreateWithraw {

    @ApiModelProperty("访问密钥")
    private String access_key;

    @ApiModelProperty("时间戳")
    private Long tonce;

    @ApiModelProperty("签名,不填(后台生成自动填)")
    private String signature;

    @ApiModelProperty(value = "只接受btc", example = "btc")
    private String currency;

    @ApiModelProperty("提款金额")
    private BigDecimal sum;

    @ApiModelProperty("加密货币地址。 现在只有比特币地址")
    private String address;

    @ApiModelProperty("矿工费。 费率从0.0001到0.001 / Kb")
    private BigDecimal fee;

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Long getTonce() {
        return tonce;
    }

    public void setTonce(Long tonce) {
        this.tonce = tonce;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
}

package org.elsa.fingobtb.entity.request

/**
 * @author valord577
 * @date 2018/7/17 12:13
 */
data class OrderInfo(

        /**
         * 币对 如eth_btc
         */
        val symbol: String,

        /**
         * 订单ID -1：未完成订单 | 否则查询相应订单号
         */
        val order_id: Long
)
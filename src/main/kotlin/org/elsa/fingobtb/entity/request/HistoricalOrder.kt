package org.elsa.fingobtb.entity.request

/**
 * @author valord577
 * @date 2018/7/16 19:09
 */
data class HistoricalOrder(

        /**
         * 􏰧􏰨􏰩􏰪􏰡􏰫􏰬交易􏰡􏰭币对 如eth_btc
         */
        val symbol: String,

        /**
         * 查询状态 0：未完成订单 | 1：已完成订单 (最近两天对数据)
         */
        val status: Int,

        /**
         * 当前页数
         */
        val current_page: Int,

        /**
         * 每夜数据条数 最多不超过200
         */
        val page_length: Int
)
package org.elsa.fingobtb.entity.request

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
@ApiModel("创建订单-入")
data class CreateOrder(

        @ApiModelProperty("访问密钥")
        var access_key: String? = null,

        @ApiModelProperty("时间戳")
        var tonce: Long? = null,

        @ApiModelProperty("签名")
        var signature: String? = null,

        @ApiModelProperty("市场id")
        var market: String? = null,

        @ApiModelProperty("sell或者buy")
        var side: String? = null,

        @ApiModelProperty("用户想要出售/购买的金额")
        var volume: String? = null,

        @ApiModelProperty("每个单位的价格")
        var price: String? = null,

        @ApiModelProperty("ord_type")
        var ord_type: String? = null

)
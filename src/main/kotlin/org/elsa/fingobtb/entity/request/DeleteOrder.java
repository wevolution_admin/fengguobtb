package org.elsa.fingobtb.entity.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("删除订单")
public class DeleteOrder {

    @ApiModelProperty("访问密钥")
    private String access_key;

    @ApiModelProperty("时间戳")
    private Long tonce;

    @ApiModelProperty("签名")
    private String signature;

    @ApiModelProperty("订单id")
    private Integer id;

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public Long getTonce() {
        return tonce;
    }

    public void setTonce(Long tonce) {
        this.tonce = tonce;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

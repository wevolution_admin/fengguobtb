package org.elsa.fingobtb.entity.request

/**
 * @author valord577
 * @date 2018/7/16 19:51
 */
data class DoTrade(

        /**
         * 币对 如eth_btc
         */
        val symbol: String,

        /**
         * 买卖类型 限价单(buy / sell ) | 市价单(buy_market / sell_market)
         */
        val type: String,

        /**
         * 下单价格 市价单不传price
         */
        val price: String?,

        /**
         * 交易数量 市价单不传amount
         */
        val amount: String?
)
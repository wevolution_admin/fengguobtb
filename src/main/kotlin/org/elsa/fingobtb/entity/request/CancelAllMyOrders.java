package org.elsa.fingobtb.entity.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("取消订单-入")
public class CancelAllMyOrders {

    @ApiModelProperty("访问密钥")
    private String access_key;

    @ApiModelProperty("时间戳")
    private Long tonce;

    @ApiModelProperty("签名")
    private String signature;

    @ApiModelProperty("如果存在，则仅取消卖出订单（卖出）或买入订单（出价）")
    private String side;

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public Long getTonce() {
        return tonce;
    }

    public void setTonce(Long tonce) {
        this.tonce = tonce;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }
}

package org.elsa.fingobtb.entity.response

/**
 * @author valord577
 * @date 2018/7/20 12:25
 */
class OrderResult {

    data class Success(
            val result: Boolean,
            val order_id: String
    ) {
        override fun toString(): String {
            return "OrderResult(result=$result, order_id='$order_id')"
        }
    }


    data class Error(
            val result: Boolean,
            val error_code: Int
    ) {
        override fun toString(): String {
            return "Error(result=$result, error_code='$error_code')"
        }
    }

}
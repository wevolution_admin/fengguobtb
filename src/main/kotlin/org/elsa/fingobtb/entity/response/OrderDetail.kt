package org.elsa.fingobtb.entity.response

/**
 * @author valord577
 * @date 2018/7/20 12:36
 */
data class OrderDetail(
        val result: String,
        val orders: List<Order>
) {
    data class Order(
            val amount: String,
            val deal_amount: String
    )
}
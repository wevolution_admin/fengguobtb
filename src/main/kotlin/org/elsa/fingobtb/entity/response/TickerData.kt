package org.elsa.fingobtb.entity.response


/**
 * @author Zhangyang Hu
 */
data class TickerData (
        val date: Long,
        val ticker: Ticker
) {
    data class Ticker(
            val volume: Double,
            val high: Double,
            val last: Double,
            val low: Double,
            val buy: Double,
            val sell: Double,
            val open: Double
    )
}
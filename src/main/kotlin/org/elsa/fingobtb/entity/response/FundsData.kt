package org.elsa.fingobtb.entity.response

/**
 * 查询余额
 *
 * @author valord577
 * @date 18-7-27 下午3:50
 */
data class FundsData(
        val result: Boolean,
        val info: Info
) {
    data class Info(
            val funds: Founds
    ) {
        data class Founds(
                val free: Free
        ) {
            data class Free(
                    val eth: String
            )
        }
    }
}
package org.elsa.fingobtb.entity.response

/**
 * 用于解析RMB与其他币种的换算
 *
 * @author valord577
 * @date 2018/7/19 18:26
 */
data class RmbPrice(
        val code: Int,
        val msg: String?,
        val timestamp: String,
        val error: String?,
        val data: List<RmbPriceData>
) {

    data class RmbPriceData(
            val symbol: String,
            val highPrice: Double,
            val lowPrice: Double,
            val lastPrice: Double
    ) {
        override fun toString(): String {
            return "RmbPriceData(symbol='$symbol', highPrice=$highPrice, lowPrice=$lowPrice, lastPrice=$lastPrice)"
        }
    }
}
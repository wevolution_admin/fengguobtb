package org.elsa.fingobtb.entity.response

/**
 * @author valord577
 * @date 2018/7/19 20:06
 */
data class BookData(
        val asks: List<List<String>>,
        val bids: List<List<String>>
) {
    override fun toString(): String {
        return "BookData(asks=$asks, bids=$bids)"
    }
}